export interface Product {
  product_id: string
  product_name: string
  price: number
  quantity: number
  description: string
  category_id?: string
  unit_id: string
  product_pictures?: string[]
}
