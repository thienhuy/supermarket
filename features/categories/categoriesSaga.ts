import {categoryActions} from './categoriesSlice'
import * as Eff from 'redux-saga/effects'
import {AxiosError} from 'axios'
import {categoriesApi} from '../../api/categoriesApi'
import {Category} from '../../models'

const takeEvery: any = Eff.takeEvery // <-- new
const put: any = Eff.put
const call: any = Eff.call

function* getCategories() {
  try {
    const response: Category[] = yield call(categoriesApi.getAll)
    console.log('response', response)

    yield put({
      type: categoryActions.fetchCategoryListSuccess.type,
      payload: response,
    })
  } catch (err) {
    const errorResponse = err as AxiosError
    console.log('err', err)
    console.log('err.response', errorResponse?.response)
  }
}

export function* categoriesSaga() {
  yield takeEvery(categoryActions.fetchCategoryList.type, getCategories)
}
