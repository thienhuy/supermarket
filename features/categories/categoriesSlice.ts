import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../../app/store'
import {Category} from '../../models'

export interface CategoryState {
  pending: boolean
  list: Category[]
  error: boolean
}

const initialState: CategoryState = {
  pending: false,
  list: [],
  error: false,
}

const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {
    fetchCategoryList(state) {
      console.log('start')
      state.pending = true
    },
    fetchCategoryListSuccess(state, action: PayloadAction<Category[]>) {
      state.pending = false
      state.list = action.payload
    },
    fetchCategoryListFailed(state) {
      state.pending = false
      state.error = true
    },
  },
})

// Actions
export const categoryActions = categoriesSlice.actions

// Selectors
export const selectCategoryList = (state: RootState) => state.categories.list
export const selectCategoryPending = (state: RootState) =>
  state.categories.pending
export const selectCategoryError = (state: RootState) => state.categories.error

// Reducer
export default categoriesSlice.reducer
