import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../../app/store'
import {Contact} from '../../models'

export interface ContactsState {
  pending: boolean
  list: Contact[]
  error: boolean
}

const initialState: ContactsState = {
  pending: false,
  list: [],
  error: false,
}

const contactsSlice = createSlice({
  name: 'contacts',
  initialState,
  reducers: {
    fetchContactList(state) {
      state.pending = true
    },
    fetchContactListSuccess(state, action: PayloadAction<Contact[]>) {
      state.pending = false
      state.list = action.payload
    },
    fetchContactListFailed(state) {
      state.pending = false
      state.error = true
    },
  },
})

// Actions
export const contactActions = contactsSlice.actions

// Selectors
export const selectContactList = (state: RootState) => state.contacts.list
export const selectContactPending = (state: RootState) => state.contacts.pending
export const selectContactError = (state: RootState) => state.contacts.error

// Reducer
export default contactsSlice.reducer
