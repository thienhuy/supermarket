import * as Eff from 'redux-saga/effects'
import {AxiosError} from 'axios'
import {contactApi} from '../../api/contactApi'
import {Contact} from '../../models'
import {contactActions} from './contactsSlice'

const takeEvery: any = Eff.takeEvery // <-- new
const put: any = Eff.put
const call: any = Eff.call

function* getContacts() {
  try {
    const response: Contact[] = yield call(contactApi.getAll)
    yield put(contactActions.fetchContactListSuccess(response))
  } catch (err) {
    const errorResponse = err as AxiosError
    console.log('err', err)
    console.log('err.response', errorResponse?.response)
  }
}

export function* contactsSaga() {
  yield takeEvery(contactActions.fetchContactList.type, getContacts)
}
