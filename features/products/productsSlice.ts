import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../../app/store'
import {Product} from '../../models'

export interface ProductsState {
  pending: boolean
  list: Product[]
  error: boolean
}

const initialState: ProductsState = {
  pending: false,
  list: [],
  error: false,
}

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    fetchProductList(state) {
      console.log('start')
      state.pending = true
    },
    fetchProductListSuccess(state, action: PayloadAction<Product[]>) {
      state.pending = false
      state.list = action.payload
    },
    fetchProductListFailed(state) {
      state.pending = false
      state.error = true
    },
  },
})

// Actions
export const productActions = productsSlice.actions

// Selectors
export const selectProductList = (state: RootState) => state.products.list
export const selectProductPending = (state: RootState) => state.products.pending
export const selectProductError = (state: RootState) => state.products.error

// Reducer
export default productsSlice.reducer
