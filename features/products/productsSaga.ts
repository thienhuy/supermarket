import * as Eff from 'redux-saga/effects'
import {AxiosError} from 'axios'
import {productsApi} from '../../api/productsApi'
import {Product} from '../../models'
import {productActions} from './productsSlice'

const takeEvery: any = Eff.takeEvery // <-- new
const put: any = Eff.put
const call: any = Eff.call

function* getProducts() {
  try {
    const response: Product[] = yield call(productsApi.getAll)
    console.log('response', response)

    yield put({
      type: productActions.fetchProductListSuccess.type,
      payload: response,
    })
  } catch (err) {
    const errorResponse = err as AxiosError
    console.log('err', err)
    console.log('err.response', errorResponse?.response)
  }
}

export function* productsSaga() {
  yield takeEvery(productActions.fetchProductList.type, getProducts)
}
