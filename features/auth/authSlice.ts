import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../../app/store'
import {User} from '../../models'
import {CookiesStorage} from '../../shared/config/cookie'

export interface LoginRequestPayload {
  data: {
    username: string
    password: string
  }

  onSuccess(): void
}

export interface RegisterRequestPayload {
  data: User

  onSuccess(): void
}

export interface RefreshSuccessPayload {
  new_token: string
}

export interface LoginSuccessPayload {
  user: {
    username: string
    first_name: string
    last_name: string
    email: string
  }
  token: string
}

export interface RegisterSuccessPayload {
  username: string
  first_name: string
  last_name: string
  email: string
}

export interface LogoutPayload {
  onSuccess(): void
}

export interface LoginState {
  pending: boolean
  error: boolean
  isAuthenticated: boolean
  errorRegister: RegisterFailedPayload
  isUserCreated: boolean
}

const initialState: LoginState = {
  pending: false,
  error: false,
  isAuthenticated: false,
  errorRegister: [],
  isUserCreated: false,
}

export type RegisterFailedPayload = FieldError[]

export type FieldError = [string, string[]]

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login(state, _: PayloadAction<LoginRequestPayload>) {
      state.pending = true
      state.error = false
    },
    loginSuccess(state, action: PayloadAction<LoginSuccessPayload>) {
      state.isAuthenticated = true
      state.pending = false
      state.error = false
      CookiesStorage.setAccessToken(action.payload.token)
      CookiesStorage.setCookieData('user', JSON.stringify(action.payload.user))
    },
    loginFailed(state) {
      state.pending = false
      state.error = true
    },
    logout(state, _: PayloadAction<LogoutPayload>) {
      state.pending = true
    },
    logoutSuccess(state, action: PayloadAction<LogoutPayload>) {
      state.isAuthenticated = false
      state.pending = false
      CookiesStorage.clearAccessToken()
      action.payload.onSuccess()
    },
    logoutFailed(state) {
      state.pending = false
      state.error = true
    },
    register(state, _: PayloadAction<RegisterRequestPayload>) {
      state.pending = true
    },
    registerSuccess(state) {
      state.pending = false
      state.isUserCreated = true
    },
    registerFailed(state, action: PayloadAction<RegisterFailedPayload>) {
      state.pending = false
      state.errorRegister = action.payload
    },
    clearError(state) {
      state.error = false
      state.isUserCreated = false
    },
    refresh(state) {
      state.pending = true
    },
    refreshSuccess(state, action: PayloadAction<RefreshSuccessPayload>) {
      state.pending = false
      CookiesStorage.setAccessToken(action.payload.new_token)
    },
    refreshFailed(state) {
      state.pending = false
    },
  },
})

// Actions
export const authActions = authSlice.actions

// Selectors
export const selectIsAuthenticated = (state: RootState) =>
  state.auth.isAuthenticated
export const selectIsError = (state: RootState) => state.auth.error
export const selectIsPending = (state: RootState) => state.auth.pending
export const selectErrorRegister = (state: RootState) =>
  state.auth.errorRegister
export const selectIsUserCreated = (state: RootState) =>
  state.auth.isUserCreated
// Reducer
export default authSlice.reducer
