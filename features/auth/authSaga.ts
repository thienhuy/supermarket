import * as Eff from 'redux-saga/effects'
import {authApi} from '../../api/authApi'
import {PayloadAction} from '@reduxjs/toolkit'
import {AxiosError} from 'axios'
import {
  authActions,
  LoginRequestPayload,
  LoginSuccessPayload,
  LogoutPayload,
  RefreshSuccessPayload,
  RegisterFailedPayload,
  RegisterRequestPayload,
} from './authSlice'

const takeEvery: any = Eff.takeEvery // <-- new
const put: any = Eff.put
const call: any = Eff.call
const all: any = Eff.all

function* login(action: PayloadAction<LoginRequestPayload>) {
  try {
    const response: LoginSuccessPayload = yield call(
      authApi.login,
      action.payload.data
    )
    yield put(authActions.loginSuccess(response))
    action.payload.onSuccess()
  } catch (err) {
    const errorResponse = err as AxiosError
    console.log('err', errorResponse?.response)
    yield put(authActions.loginFailed())
  }
}

function* register(action: PayloadAction<RegisterRequestPayload>) {
  try {
    yield call(authApi.register, action.payload.data)
    yield put(authActions.registerSuccess())
    action.payload.onSuccess()
  } catch (err) {
    const errorResponse = err as AxiosError

    if (errorResponse?.response?.status === 400) {
      yield put(
        authActions.registerFailed(
          Object.entries(errorResponse?.response?.data) as RegisterFailedPayload
        )
      )
    }
  }
}

function* logout(action: PayloadAction<LogoutPayload>) {
  try {
    yield call(authApi.logout)
    yield put(authActions.logoutSuccess(action.payload))
  } catch (err) {
    yield put(authActions.logoutFailed())
  }
}

function* refresh() {
  try {
    const payload: RefreshSuccessPayload = yield call(authApi.refresh)
    yield put(authActions.refreshSuccess(payload))
  } catch (err) {
    yield put(authActions.refreshFailed())
  }
}

export function* authSaga() {
  yield all([
    takeEvery(authActions.login.type, login),
    takeEvery(authActions.register.type, register),
    takeEvery(authActions.logout.type, logout),
    takeEvery(authActions.refresh.type, refresh),
  ])
}
