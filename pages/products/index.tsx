import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import {
  Alert,
  Avatar,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Drawer,
  Snackbar,
  Stack,
  useMediaQuery,
  useTheme
} from '@mui/material'
import IconButton from '@mui/material/IconButton'
import Tooltip from '@mui/material/Tooltip'
import { DataGrid, GridColDef } from '@mui/x-data-grid'
import { useRouter } from 'next/router'
import React, { Fragment, useEffect, useState } from 'react'
import { productsApi } from '../../api/productsApi'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  productActions,
  selectProductList
} from '../../features/products/productsSlice'
import { CookiesStorage } from '../../shared/config/cookie'
import { removeAscent } from '../../shared/utils/validate'
import { UserListToolbar } from '../../src/components/contact'
import Layout from '../../src/components/dashboard/Layout'
import ProductComponent from '../../src/components/product/ProductComponent'

export interface Obj {
  right: boolean
}

export default function Products() {
  const router = useRouter()
  const theme = useTheme()
  const dispatch = useAppDispatch()
  const [filterName, setFilterName] = useState('')
  const [idContact, setIdContact] = useState<string>('0')
  const [open, setOpen] = useState<boolean>(false)
  const [openDialog, setOpenDialog] = useState<boolean>(false)
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'))
  const [deleteId, setDeleteId] = useState<string>()

  const [state, setState] = useState<Obj>({
    right: false,
  })
  const productList = useAppSelector(selectProductList)
  console.log('product list: ', productList)

  useEffect(() => {
    if (!CookiesStorage.authenticated()) {
      router.push('/login')
    } else {
      dispatch(productActions.fetchProductList())
    }
  }, [])

  const handleOpenDialog = (id: string) => {
    setOpenDialog(true)
    setDeleteId(id)
  }

  const handleCloseDialog = () => {
    setOpenDialog(false)
  }

  const toggleDrawer =
    (anchor: 'right', open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return
      }

      setState({...state, [anchor]: open})
    }

  const handleFilterByName = (event: any) => {
    setFilterName(event.target.value)
  }

  const handleOpenEdit = (id: string) => {
    console.log('edit')
    setState({right: true})
    setIdContact(id)
  }

  const handleDeleteProduct = async (id: string = '-1') => {
    if (id === '-1') return
    try {
      await productsApi.remove(id)
    } catch (error) {
      console.log(error)
    }
    onDone()
  }

  const handleOpenAdd = () => {
    setState({right: true})
    setIdContact('')
  }

  const onDone = () => {
    setOpen(true)
    setState({right: false})
    handleCloseDialog()
    dispatch(productActions.fetchProductList())
  }

  const filterToLowCase = (query: string) => {
    let result = false
    if (query)
      result = removeAscent(query)
        .toLowerCase()
        .includes(removeAscent(filterName).toLowerCase())

    return result
  }

  const columns: GridColDef[] = [
    {field: 'id', headerName: 'ID', width: 70},
    {
      field: 'product_name',
      headerName: 'Product-name',
      width: 150,
    },
    {
      field: 'price',
      headerName: 'Price',
      width: 120,
    },
    {
      field: 'description',
      headerName: 'Description',
      width: 180,
    },
    {
      field: 'product_pictures',
      headerName: 'product_pictures',
      width: 130,
      renderCell: (params: any) => (
        // console.log('params', params);
        <Avatar
          src={'http://localhost:3000' + params?.value[0]}
          alt={'avatar'}
          sx={{width: 50, height: 50}}
        />
      ),
    },
    {
      field: 'quantity',
      headerName: 'Quantity',
      width: 100,
    },
    {
      field: 'category_id',
      headerName: 'CategoryId',
      width: 120,
    },
    {
      field: 'unit_id',
      headerName: 'UnitId',
      width: 80,
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 120,
      renderCell: (params: any) => (
        <>
          <Button
            color='primary'
            onClick={() => {
              handleOpenEdit(`${params.id}`)
            }}>
            <EditIcon />
          </Button>
          <Tooltip title='Delete' placement='top'>
            <IconButton
              color='error'
              onClick={() => handleOpenDialog(`${params.id}`)}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </>
      ),
    },
  ]

  const handleClose = (_?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }
  return (
    <Fragment>
      <Layout>
        <Stack
          direction='row'
          alignItems='flex-end'
          justifyContent='space-between'
          mb={2}>
          <UserListToolbar
            filterName={filterName}
            onFilterName={handleFilterByName}
          />
          <Button variant='outlined' color='info' onClick={handleOpenAdd}>
            <AddCircleOutlineIcon />
            &nbsp; ADD PRODUCTS
          </Button>
        </Stack>
        <DataGrid
          autoHeight
          rowHeight={70}
          rows={productList.filter((item) =>
            filterToLowCase(item.product_name)
          )}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          checkboxSelection
          disableSelectionOnClick
          onSelectionModelChange={(model: any) => console.log('model', model)}
        />

        {/*Toast message*/}
        <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
          <Alert severity='success' sx={{width: '100%'}} onClose={handleClose}>
            Invalid credentials!
          </Alert>
        </Snackbar>
        {/*Dialog*/}
        <Dialog
          fullScreen={fullScreen}
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby='responsive-dialog-title'>
          <DialogTitle id='responsive-dialog-title'>
            {'Are you sure to delete that contact?'}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              You won't be able to revert that contact
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleCloseDialog}>
              Cancel
            </Button>
            <Button onClick={() => handleDeleteProduct(deleteId)} autoFocus>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </Layout>
      {/*****************************************************************************/}
      <Drawer
        anchor='right'
        open={state.right}
        onClose={toggleDrawer('right', false)}>
        <ProductComponent id={idContact} onDone={onDone} />
      </Drawer>
    </Fragment>
  )
}
