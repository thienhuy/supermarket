// import * as React from 'react'
// import {useEffect, useState} from 'react'
// import Container from '@mui/material/Container'
// import {Button, TextField} from '@mui/material'
// import {productsApi} from '../api/productsApi'

// export default function Index() {
//   const [form, setForm] = useState<any>({})
//   const handleChange = (e: any) => {
//     const {value, name} = e.target
//     setForm({...form, [name]: value})
//   }

//   useEffect(() => {
//     productsApi
//       .getById('618342043f5806a44eaeb23d')
//       .then(({deletedAt, ...res}) => setForm(res))
//       .catch((err) => console.log('err', err))
//   }, [])

//   const onClick = () => {
//     const formData = new FormData()
//     Object.entries(form).forEach(([name, value]) => {
//       if (name === 'product_pictures') {
//         formData.append(name, JSON.stringify(value))
//       } else if (name === 'file[]') {
//         for (let file of value as File[]) {
//           formData.append('file[]', file)
//         }
//       } else {
//         formData.append(name, value as any)
//       }
//     })

//     productsApi
//       .update('618342043f5806a44eaeb23d', formData as any)
//       .then((res) => console.log('res', res))
//   }
//   const handleFileChange = (e: any) => {
//     setForm({...form, 'file[]': e.target.files})
//   }

//   const handleDeleteImage = (image: string) => {
//     const {product_pictures} = form
//     const newList = product_pictures.filter((item: string) => item != image)
//     console.log('new', newList)
//     setForm({
//       ...form,
//       product_pictures: newList,
//     })
//   }

//   const {
//     product_name,
//     category_id,
//     unit_id,
//     description,
//     quantity,
//     product_pictures,
//     price,
//   } = form

//   console.log('form', form)
//   return (
//     <Container maxWidth='sm'>
//       <TextField
//         name={'product_name'}
//         label='Product name'
//         onChange={handleChange}
//         value={product_name}
//       />
//       <TextField
//         name={'category_id'}
//         label={'Category id'}
//         onChange={handleChange}
//         value={category_id}
//       />
//       <TextField
//         name={'unit_id'}
//         label={'Unit id'}
//         onChange={handleChange}
//         value={unit_id}
//       />
//       <TextField
//         name={'price'}
//         label={'Price'}
//         onChange={handleChange}
//         value={price}
//       />
//       <TextField
//         name={'description'}
//         label={'Description'}
//         onChange={handleChange}
//         value={description}
//       />
//       <TextField
//         name={'quantity'}
//         label={'Quantity'}
//         onChange={handleChange}
//         value={quantity}
//       />
//       <label htmlFor='contained-button-file'>
//         <input
//           style={{display: 'none'}}
//           accept='image/*'
//           id='contained-button-file'
//           multiple
//           type='file'
//           onChange={handleFileChange}
//         />
//         <Button variant='contained' component='span'>
//           upload more
//         </Button>
//       </label>
//       {product_pictures?.map((image: any) => (
//         <div key={image}>
//           <img
//             src={'http://localhost:3000/' + image}
//             width={300}
//             height={300}
//           />
//           <Button onClick={() => handleDeleteImage(image)}>X</Button>
//         </div>
//       ))}
//       <Button onClick={onClick}>Submit</Button>
//     </Container>
//   )
// }
import {Button, List, ListItem} from '@mui/material'
import {makeStyles} from '@mui/styles'
import React from 'react'
// import Button from '../src/components/landing/CustomButtons/Button'
import Header from '../src/components/landing/Header'
import Footer from '../src/components/landing/Footer/Footer.js'
import headerLinksStyle from '../src/components/landing/headerLinksStyle'
import {container, title} from '../src/components/landing/material-custom'
const navbarsStyle: any = (theme: any) => ({
  section: {
    padding: '70px 0',
    paddingTop: '0',
  },
  container,
  title: {
    ...title,
    marginTop: '30px',
    minHeight: '32px',
    textDecoration: 'none',
  },
  navbar: {
    marginBottom: '-10px',
    zIndex: '100',
    position: 'relative',
    overflow: 'hidden',
    '& header': {
      borderRadius: '0',
    },
  },
  navigation: {
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    marginTop: '0',
    minHeight: '600px',
  },
  formControl: {
    margin: '0 !important',
    paddingTop: '0',
  },
  inputRootCustomClasses: {
    margin: '0!important',
  },
  searchIcon: {
    width: '20px',
    height: '20px',
    color: 'inherit',
  },
  ...headerLinksStyle(theme),
  img: {
    width: '40px',
    height: '40px',
    borderRadius: '50%',
  },
  imageDropdownButton: {
    padding: '0px',
    top: '4px',
    borderRadius: '50%',
    marginLeft: '5px',
  },
})

const useStyles = makeStyles(navbarsStyle)
function LandingPage(props: any) {
  const classes = useStyles(props)
  return (
    <div id='navbar' className={classes.navbar}>
      <div
        className={classes.navigation}
        style={{backgroundImage: 'url(/images/bg1.jpg)'}}>
        <Header
          brand='Pi-SuperMarket'
          color='transparent'
          rightLinks={
            <List className={classes.list}>
              <ListItem className={classes.listItem}>
                <Button color='primary' className={classes.navLink}>
                  <i
                    className={
                      classes.socialIcons +
                      ' ' +
                      classes.marginRight5 +
                      ' fab fa-twitter'
                    }
                  />{' '}
                  Twitter
                </Button>
              </ListItem>
              <ListItem className={classes.listItem}>
                <Button color='primary' className={classes.navLink}>
                  <i
                    className={
                      classes.socialIcons +
                      ' ' +
                      classes.marginRight5 +
                      ' fab fa-facebook'
                    }
                  />{' '}
                  Facebook
                </Button>
              </ListItem>
              <ListItem className={classes.listItem}>
                <Button color='primary' className={classes.navLink}>
                  <i
                    className={
                      classes.socialIcons +
                      ' ' +
                      classes.marginRight5 +
                      ' fab fa-instagram'
                    }
                  />{' '}
                  Instagram
                </Button>
              </ListItem>
            </List>
          }
        />
      </div>

      <Footer />
    </div>
  )
}

LandingPage.propTypes = {}

export default LandingPage
