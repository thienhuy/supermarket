import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import FindReplaceIcon from '@mui/icons-material/FindReplace'
import {
  Alert,
  Avatar,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Drawer,
  InputAdornment,
  OutlinedInput,
  Snackbar,
  Stack,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import {Theme} from '@mui/material/styles'
import Tooltip from '@mui/material/Tooltip'
import {DataGrid, GridColDef} from '@mui/x-data-grid'
import {useRouter} from 'next/router'
import React, {Fragment, useEffect, useState} from 'react'
import {contactApi} from '../../api/contactApi'
import {useAppDispatch, useAppSelector} from '../../app/hooks'
import {
  contactActions,
  selectContactList,
} from '../../features/contacts/contactsSlice'
import {CookiesStorage} from '../../shared/config/cookie'
import {removeAscent} from '../../shared/utils/validate'
import {EditUserComponent, UserListToolbar} from '../../src/components/contact'
import Layout from '../../src/components/dashboard/Layout'

export interface Obj {
  right: boolean
}

export default function Contacts() {
  const router = useRouter()
  const theme = useTheme()
  const dispatch = useAppDispatch()
  const [filterName, setFilterName] = useState('')
  const [idContact, setIdContact] = useState<string>('0')
  const [open, setOpen] = useState<boolean>(false)
  const [openDialog, setOpenDialog] = useState<boolean>(false)
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'))
  const [deleteId, setDeleteId] = useState<string>()

  const [state, setState] = useState<Obj>({
    right: false,
  })
  const contactList = useAppSelector(selectContactList)

  useEffect(() => {
    if (!CookiesStorage.authenticated()) {
      router.push('/login')
    } else {
      dispatch(contactActions.fetchContactList())
    }
  }, [])

  const handleOpenDialog = (id: string) => {
    setOpenDialog(true)
    setDeleteId(id)
  }

  const handleCloseDialog = () => {
    setOpenDialog(false)
  }

  const toggleDrawer =
    (anchor: 'right', open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return
      }

      setState({...state, [anchor]: open})
    }

  const handleFilterByName = (event: any) => {
    setFilterName(event.target.value)
  }

  const handleOpenEdit = (id: string) => {
    console.log('edit')
    setState({right: true})
    setIdContact(id)
  }

  const handleDeleteContact = async (id: string = '-1') => {
    if (id === '-1') return
    try {
      await contactApi.remove(id)
    } catch (error) {
      console.log(error)
    }
    onDone()
  }

  const handleOpenAdd = () => {
    setState({right: true})
    setIdContact('')
  }

  const onDone = () => {
    setOpen(true)
    setState({right: false})
    handleCloseDialog()
    dispatch(contactActions.fetchContactList())
  }

  const filterToLowCase = (query: string) => {
    let result = false
    if (query)
      result = removeAscent(query)
        .toLowerCase()
        .includes(removeAscent(filterName).toLowerCase())

    return result
  }

  const columns: GridColDef[] = [
    {field: 'id', headerName: 'ID', width: 90},
    {
      field: 'first_name',
      headerName: 'First name',
      width: 150,
    },
    {
      field: 'last_name',
      headerName: 'Last name',
      width: 150,
    },
    {
      field: 'phone_number',
      headerName: 'Phone number',
      width: 200,
    },
    {
      field: 'contact_picture',
      headerName: 'Picture',
      width: 150,
      renderCell: (params: any) => (
        <Avatar
          src={params?.value as string}
          alt={'avatar'}
          sx={{width: 50, height: 50}}
        />
      ),
    },
    {
      field: 'is_favorite',
      headerName: 'Favorite',
      type: 'boolean',
      width: 150,
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 200,
      renderCell: (params: any) => (
        <>
          <Button
            color='primary'
            onClick={() => {
              handleOpenEdit(`${params.id}`)
            }}>
            <EditIcon />
          </Button>
          <Tooltip title='Delete' placement='top'>
            <IconButton
              color='error'
              onClick={() => handleOpenDialog(`${params.id}`)}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </>
      ),
    },
  ]

  const handleClose = (_?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }
  return (
    <Fragment>
      <Layout>
        <OutlinedInput
          sx={{
            height: 40,
            p: 1,
            m: 1,
            borderRadius: 10,
            border: 1,
            borderColor: (theme: Theme) => theme.palette.primary.main,
          }}
          startAdornment={
            <InputAdornment position='start'>
              <FindReplaceIcon />
            </InputAdornment>
          }
        />
        <div>
          <Box
            sx={{
              p: 1,
              m: 1,
              borderRadius: 10,
              border: 1,
              borderColor: (theme: Theme) => theme.palette.primary.main,
            }}>
            Border color with theme value.
          </Box>
        </div>
        <Stack
          direction='row'
          alignItems='flex-end'
          justifyContent='space-between'
          mb={2}>
          <UserListToolbar
            filterName={filterName}
            onFilterName={handleFilterByName}
          />
          <Button variant='outlined' color='info' onClick={handleOpenAdd}>
            <AddCircleOutlineIcon />
            &nbsp; Add contact
          </Button>
        </Stack>
        <DataGrid
          autoHeight
          rowHeight={70}
          rows={contactList.filter((item) => filterToLowCase(item.first_name))}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          checkboxSelection
          disableSelectionOnClick
          onSelectionModelChange={(model: any) => console.log('model', model)}
        />

        {/*Toast message*/}
        <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
          <Alert severity='success' sx={{width: '100%'}} onClose={handleClose}>
            Invalid credentials!
          </Alert>
        </Snackbar>
        {/*Dialog*/}
        <Dialog
          fullScreen={fullScreen}
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby='responsive-dialog-title'>
          <DialogTitle id='responsive-dialog-title'>
            {'Are you sure to delete that contact?'}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              You won't be able to revert that contact
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleCloseDialog}>
              Cancel
            </Button>
            <Button onClick={() => handleDeleteContact(deleteId)} autoFocus>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </Layout>
      {/*****************************************************************************/}
      <Drawer
        anchor='right'
        open={state.right}
        onClose={toggleDrawer('right', false)}>
        <EditUserComponent id={idContact} onDone={onDone} />
      </Drawer>
    </Fragment>
  )
}
