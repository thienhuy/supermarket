import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import Link from 'next/link'
import Paper from '@mui/material/Paper'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import {createTheme, ThemeProvider} from '@mui/material/styles'
import {useAppDispatch, useAppSelector} from '../../app/hooks'

import {
  authActions,
  selectErrorRegister,
  selectIsAuthenticated,
} from '../../features/auth/authSlice'
import * as yup from 'yup'
import {yupResolver} from '@hookform/resolvers/yup'
import {SubmitHandler, useForm} from 'react-hook-form'
import {InputField} from '../../src/components/FormFields'
import {User} from '../../models'
import {useRouter} from 'next/router'
import {useEffect} from 'react'

interface IFormInputs {
  username: string
  password: string
}

type FieldNames = 'username' | 'password'

const theme = createTheme()

export default function SignInSide() {
  const dispatch = useAppDispatch()
  const router = useRouter()
  const isAuthenticated = useAppSelector(selectIsAuthenticated)
  const errorRegister = useAppSelector(selectErrorRegister)
  console.log('errorRegister', errorRegister)

  useEffect(() => {
    if (errorRegister.length) {
      errorRegister.forEach(([field, errorList]) => {
        setError(field as FieldNames, {type: 'manual', message: errorList[0]})
      })
    }
  }, [errorRegister])

  const onSubmit: SubmitHandler<IFormInputs> = (userData) => {
    const data = userData as User
    dispatch(
      authActions.register({
        data,
        onSuccess() {
          router.push('/login')
        },
      })
    )
  }
  const schema = yup.object({
    username: yup
      .string()
      .matches(/^[\w.@+-]+$/, 'Username is valid!')
      .max(255)
      .min(2)
      .required(),
    first_name: yup.string().min(2).max(255).required(),
    last_name: yup.string().min(2).max(255).required(),
    email: yup.string().email(),
    password: yup.string().max(65).min(8).required(),
  })
  const {handleSubmit, control, setError} = useForm<IFormInputs>({
    resolver: yupResolver(schema),
  })
  console.log(isAuthenticated)

  return (
    <ThemeProvider theme={theme}>
      <Grid container component='main' sx={{height: '100vh'}}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: 'url(https://source.unsplash.com/random)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: (t) =>
              t.palette.mode === 'light'
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component='h1' variant='h5'>
              Register
            </Typography>
            <Box
              component='form'
              noValidate
              onSubmit={handleSubmit(onSubmit)}
              sx={{mt: 1}}>
              <InputField
                name={'username'}
                label={'Username'}
                control={control}
              />
              <Grid item container spacing={2}>
                <Grid item sm={6} xs={12}>
                  <InputField
                    name={'first_name'}
                    label={'First name'}
                    control={control}
                  />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <InputField
                    name={'last_name'}
                    label={'Last name'}
                    control={control}
                  />
                </Grid>
              </Grid>
              <InputField
                name={'email'}
                label={'Email address'}
                control={control}
              />
              <InputField
                name={'password'}
                type={'password'}
                label={'Password'}
                control={control}
              />
              <FormControlLabel
                control={<Checkbox value='remember' color='primary' />}
                label='Remember me'
              />
              <Button
                type='submit'
                fullWidth
                variant='contained'
                sx={{mt: 3, mb: 2}}>
                Sign In
              </Button>
              <Grid container>
                <Grid item>
                  <Link href='/login'>{'Already have a account? Sign In'}</Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  )
}
