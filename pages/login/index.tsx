import {yupResolver} from '@hookform/resolvers/yup'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import VisibilityIcon from '@mui/icons-material/Visibility'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'
import {Alert} from '@mui/lab'
import LoadingButton from '@mui/lab/LoadingButton'
import {IconButton, InputAdornment, Snackbar} from '@mui/material'
import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import CssBaseline from '@mui/material/CssBaseline'
import FormControlLabel from '@mui/material/FormControlLabel'
import Grid from '@mui/material/Grid'
import Checkbox from '@mui/material/Checkbox'
import Link from 'next/link'
import Paper from '@mui/material/Paper'
import {createTheme, ThemeProvider} from '@mui/material/styles'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import {useRouter} from 'next/router'
import React, {useState} from 'react'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import {useAppDispatch, useAppSelector} from '../../app/hooks'
import {
  authActions,
  selectIsAuthenticated,
  selectIsError,
  selectIsPending,
  selectIsUserCreated,
} from '../../features/auth/authSlice'
import {CookiesStorage} from '../../shared/config/cookie'

interface IFormInputs {
  username: string
  password: string
}

function Copyright(props: any) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}>
      {'Copyright © '}
      <Link href='https://material-ui.com/'>Your Website</Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
}

const theme = createTheme()

export default function SignInSide() {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const isAuthenticated = useAppSelector(selectIsAuthenticated)
  const isError = useAppSelector(selectIsError)
  const isPending = useAppSelector(selectIsPending)
  const isUserCreated = useAppSelector(selectIsUserCreated)
  const [showPassword, setShowPassword] = useState(false)

  const handleClose = (_?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }
    dispatch(authActions.clearError())
  }

  const onSubmit: SubmitHandler<IFormInputs> = (data) => {
    dispatch(
      authActions.login({
        data,
        onSuccess() {
          router.push('/contacts')
        },
      })
    )
  }
  const schema = yup.object({
    username: yup.string().max(255).min(2).required(),
    password: yup.string().max(65).min(8).required(),
  })

  const {handleSubmit, control} = useForm<IFormInputs>({
    resolver: yupResolver(schema),
  })

  console.log('isAuthenticated', isAuthenticated)

  if (CookiesStorage.authenticated()) {
    router.push('/contacts')
  }

  const handleShowPassword = () => {
    setShowPassword((show) => !show)
  }

  return (
    <ThemeProvider theme={theme}>
      <Grid container component='main' sx={{height: '100vh'}}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: 'url(https://source.unsplash.com/random)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: (t) =>
              t.palette.mode === 'light'
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component='h1' variant='h5'>
              Sign in
            </Typography>
            <Box
              component='form'
              noValidate
              onSubmit={handleSubmit(onSubmit)}
              sx={{mt: 1}}>
              <Controller
                name={'username'}
                control={control}
                defaultValue={''}
                render={({field, formState: {errors}}) => (
                  <TextField
                    margin='normal'
                    required
                    fullWidth
                    id='email'
                    label='Email Address'
                    autoComplete='Username'
                    autoFocus
                    error={!!errors?.username}
                    helperText={errors?.username?.message?.toUpperCase()}
                    {...field}
                  />
                )}
              />
              <Controller
                control={control}
                name={'password'}
                defaultValue={''}
                render={({field, formState: {errors}}) => (
                  <TextField
                    margin='normal'
                    required
                    fullWidth
                    label='Password'
                    type={showPassword ? 'text' : 'password'}
                    id='password'
                    autoComplete='current-password'
                    {...field}
                    error={!!errors?.password}
                    helperText={errors?.password?.message?.toUpperCase()}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position='end'>
                          <IconButton onClick={handleShowPassword} edge='end'>
                            {showPassword ? (
                              <VisibilityIcon />
                            ) : (
                              <VisibilityOffIcon />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
              <FormControlLabel
                control={<Checkbox value='remember' color='primary' />}
                label='Remember me'
              />
              <LoadingButton
                type='submit'
                loading={isPending}
                fullWidth
                variant='contained'
                sx={{mt: 3, mb: 2}}>
                Sign In
              </LoadingButton>
              <Grid container>
                <Grid item xs>
                  <Link href='#'>Forgot password?</Link>
                </Grid>
                <Grid item>
                  <Link href='/register'>
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
              <Copyright sx={{mt: 5}} />
            </Box>
          </Box>
        </Grid>
      </Grid>
      <Snackbar open={isError} autoHideDuration={6000} onClose={handleClose}>
        <Alert severity='error' sx={{width: '100%'}} onClose={handleClose}>
          Invalid credentials!
        </Alert>
      </Snackbar>
      <Snackbar
        open={isUserCreated}
        autoHideDuration={6000}
        onClose={handleClose}>
        <Alert severity='success' sx={{width: '100%'}} onClose={handleClose}>
          Register successfully!
        </Alert>
      </Snackbar>
    </ThemeProvider>
  )
}
