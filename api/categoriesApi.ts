import axiosClient from './axiosClient'
import {Category} from '../models'

export const categoriesApi = {
  getAll(): Promise<Category[]> {
    return axiosClient.get('/categories')
  },

  update(data: Partial<Category>): Promise<Category> {
    const url = `/categories/${data.category_id}`
    return axiosClient.patch(url, data)
  },

  add(data: Category): Promise<Category> {
    const url = '/categories'
    return axiosClient.post(url, data)
  },

  remove(id: string): Promise<any> {
    const url = `/categories/${id}`
    return axiosClient.delete(url)
  },
}
