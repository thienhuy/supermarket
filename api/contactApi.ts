import axiosClient from './axiosClient'
import {Contact} from '../models'

export const contactApi = {
  getAll(): Promise<Contact[]> {
    return axiosClient.get('/contacts')
  },
  update(data: Partial<Contact>): Promise<Contact> {
    const url = `/contacts/${data.id}`
    return axiosClient.patch(url, data)
  },
  getById(id: string): Promise<Contact> {
    const url = `/contacts/${id}`
    return axiosClient.get(url)
  },

  add(data: Contact): Promise<Contact> {
    const url = '/contacts'
    return axiosClient.post(url, data)
  },

  remove(id: string): Promise<any> {
    const url = `/contacts/${id}`
    return axiosClient.delete(url)
  },
}
