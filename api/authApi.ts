import axiosClient from './axiosClient'
import {User} from '../models'
import {
  LoginSuccessPayload,
  RegisterSuccessPayload,
} from '../features/auth/authSlice'

export const authApi = {
  login(data: User): Promise<LoginSuccessPayload> {
    return axiosClient.post('/auth/login', data)
  },
  register(data: User): Promise<RegisterSuccessPayload> {
    return axiosClient.post('/auth/register', data)
  },
  logout(): Promise<any> {
    return axiosClient.delete('/auth/logout')
  },
  refresh() {
    return axiosClient.post('/auth/refresh')
  },
}
