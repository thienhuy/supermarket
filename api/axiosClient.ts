import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {CookiesStorage} from '../shared/config/cookie'
import Router from 'next/router'
import jwt_decode from 'jwt-decode'
import {store} from '../app/store'
import {authActions} from '../features/auth/authSlice'

console.log('api_url', typeof process.env.API_URL)
const axiosClient = axios.create({
  baseURL: 'http://localhost:3000/api',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
})

const FIVE_MINUTES = 5 * 60 * 1000

const refreshIfNeed = () => {
  const token = CookiesStorage.getAccessToken() as string
  if (token) {
    const {exp} = jwt_decode(token) as any
    const expiredTime = exp * 1000
    if (
      expiredTime - Date.now() > 0 &&
      expiredTime - Date.now() < FIVE_MINUTES
    ) {
      store.dispatch(authActions.refresh())
    }
  }
}

// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    const newConfig = {...config}
    if (!newConfig.url?.includes('auth/refresh')) {
      refreshIfNeed()
    }
    if (CookiesStorage.authenticated()) {
      newConfig.headers = {
        ...newConfig.headers,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${CookiesStorage.getAccessToken()}`,
      }
    }
    console.log('newConfig', newConfig)
    return newConfig
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
axiosClient.interceptors.response.use(
  function (response: AxiosResponse) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response.data
  },
  function (error) {
    if (error.response?.status === 401) {
      CookiesStorage.clearAccessToken()
      CookiesStorage.clearCookieData('user')
      Router.push('/login')
    }
    return Promise.reject(error)
  }
)

export default axiosClient
