import axiosClient from './axiosClient'
import {Product} from '../models'

const config = {
  headers: {
    'Content-Type': 'multipart/form-data',
  },
}

export const productsApi = {
  getAll(page: number): Promise<Product[]> {
    return axiosClient.get(`/products?page=${page}`)
  },

  update(id: string, data: any): Promise<Product> {
    const url = `/products/${id}`
    return axiosClient.patch(url, data, config)
  },

  getById(id: string): Promise<any> {
    const url = `/products/${id}`
    return axiosClient.get(url)
  },

  add(data: any): Promise<Product> {
    const url = '/products'
    return axiosClient.post(url, data, config)
  },

  remove(id: string): Promise<any> {
    const url = `/products/${id}`
    return axiosClient.delete(url)
  },
}
