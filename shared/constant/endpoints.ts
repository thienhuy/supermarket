export const URL_LOGIN = '/api/v1/auth/login';
export const URL_VERIFY_EMAIL_REGISTER = 'api/v1/auth/register/verify';
export const URL_RESENT_VERIFY_EMAIL_REGISTER = 'api/v1/auth/register';
export const URL_FORGOT_PASSWORD = 'api/v1/auth/passwords/reset/request';
export const URL_VERIFY_EMAIL_RESET_PASSWORD = 'api/v1/auth/passwords/reset/verify';
export const URL_RESET_PASSWORD = 'api/v1/auth/passwords/reset';
export const URL_LOGOUT = 'api/v1/auth/logout';
