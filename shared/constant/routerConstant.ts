export const constants = {
  RESET_PASSWORD: '/account/reset-password',
  FORGET_PASSWORD: '/account/forget-password',
};
