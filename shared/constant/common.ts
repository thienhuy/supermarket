const DefaultMenu = {
  activatedMenuItemIds: [1],
  menuItems: [
    {
      path: '/contacts',
      name: 'Dashboard',
      icon: {
        propTypes: {},
        displayName: 'Home',
      },
      header: 'Navigation',
      badge: {
        variant: 'success',
        text: '1',
      },
      component: {
        _status: 0,
        _result: {},
      },
      roles: ['Admin'],
      id: 1,
      active: false,
    },
    {
      path: '/apps/calendar',
      name: 'Calendar',
      header: 'Apps',
      icon: {
        propTypes: {},
        displayName: 'Calendar',
      },
      component: {
        _status: -1,
        _result: null,
      },
      roles: ['Admin'],
      id: 2,
      active: false,
    },
    {
      path: '/apps/email',
      name: 'Email',
      icon: {
        propTypes: {},
        displayName: 'Inbox',
      },
      children: [
        {
          path: '/apps/email/inbox',
          name: 'Inbox',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 4,
          parentId: 3,
          active: false,
        },
        {
          path: '/apps/email/details',
          name: 'Details',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 5,
          parentId: 3,
          active: false,
        },
        {
          path: '/apps/email/compose',
          name: 'Compose',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 6,
          parentId: 3,
          active: false,
        },
      ],
      id: 3,
      active: false,
    },
    {
      path: '/apps/projects',
      name: 'Projects',
      icon: {
        propTypes: {},
        displayName: 'Briefcase',
      },
      children: [
        {
          path: '/apps/projects/list',
          name: 'List',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 8,
          parentId: 7,
          active: false,
        },
        {
          path: '/apps/projects/detail',
          name: 'Detail',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 9,
          parentId: 7,
          active: false,
        },
      ],
      id: 7,
      active: false,
    },
    {
      path: '/apps/tasks',
      name: 'Tasks',
      icon: {
        propTypes: {},
        displayName: 'Bookmark',
      },
      children: [
        {
          path: '/apps/tasks/list',
          name: 'List',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 11,
          parentId: 10,
          active: false,
        },
        {
          path: '/apps/tasks/board',
          name: 'Board',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 12,
          parentId: 10,
          active: false,
        },
      ],
      id: 10,
      active: false,
    },
    {
      path: '/pages',
      name: 'Pages',
      header: 'Custom',
      icon: {
        propTypes: {},
        displayName: 'FileText',
      },
      children: [
        {
          path: '/pages/starter',
          name: 'Starter',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 14,
          parentId: 13,
          active: false,
        },
        {
          path: '/pages/profile',
          name: 'Profile',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 15,
          parentId: 13,
          active: false,
        },
        {
          path: '/pages/activity',
          name: 'Activity',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 16,
          parentId: 13,
          active: false,
        },
        {
          path: '/pages/invoice',
          name: 'Invoice',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 17,
          parentId: 13,
          active: false,
        },
        {
          path: '/pages/pricing',
          name: 'Pricing',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 18,
          parentId: 13,
          active: false,
        },
        {
          path: '/pages/error-404',
          name: 'Error 404',
          component: {
            _status: -1,
            _result: null,
          },
          id: 19,
          parentId: 13,
          active: false,
        },
        {
          path: '/pages/error-500',
          name: 'Error 500',
          component: {
            _status: -1,
            _result: null,
          },
          id: 20,
          parentId: 13,
          active: false,
        },
      ],
      id: 13,
      active: false,
    },
    {
      path: '/ui',
      name: 'UI Elements',
      header: 'Components',
      icon: {
        propTypes: {},
        displayName: 'Package',
      },
      children: [
        {
          path: '/ui/bscomponents',
          name: 'Bootstrap UI',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 22,
          parentId: 21,
          active: false,
        },
        {
          path: '/ui/icons',
          name: 'Icons',
          children: [
            {
              path: '/ui/icons/feather',
              name: 'Feather Icons',
              component: {
                _status: -1,
                _result: null,
              },
              roles: ['Admin'],
              id: 24,
              parentId: 23,
              active: false,
            },
            {
              path: '/ui/icons/unicons',
              name: 'Unicons Icons',
              component: {
                _status: -1,
                _result: null,
              },
              roles: ['Admin'],
              id: 25,
              parentId: 23,
              active: false,
            },
          ],
          id: 23,
          parentId: 21,
          active: false,
        },
        {
          path: '/ui/widgets',
          name: 'Widgets',
          component: {
            _status: -1,
            _result: null,
          },
          roles: ['Admin'],
          id: 26,
          parentId: 21,
          active: false,
        },
      ],
      id: 21,
      active: false,
    },
    {
      path: '/charts',
      name: 'Charts',
      component: {
        _status: -1,
        _result: null,
      },
      icon: {
        propTypes: {},
        displayName: 'PieChart',
      },
      roles: ['Admin'],
      id: 27,
      active: false,
    },
    {
      path: '/forms',
      name: 'Forms',
      icon: {
        propTypes: {},
        displayName: 'FileText',
      },
      children: [
        {
          path: '/forms/basic',
          name: 'Basic Elements',
          component: {
            _status: -1,
            _result: null,
          },
          id: 29,
          parentId: 28,
          active: false,
        },
        {
          path: '/forms/advanced',
          name: 'Advanced',
          component: {
            _status: -1,
            _result: null,
          },
          id: 30,
          parentId: 28,
          active: false,
        },
        {
          path: '/forms/validation',
          name: 'Validation',
          component: {
            _status: -1,
            _result: null,
          },
          id: 31,
          parentId: 28,
          active: false,
        },
        {
          path: '/forms/wizard',
          name: 'Wizard',
          component: {
            _status: -1,
            _result: null,
          },
          id: 32,
          parentId: 28,
          active: false,
        },
        {
          path: '/forms/editor',
          name: 'Editor',
          component: {
            _status: -1,
            _result: null,
          },
          id: 33,
          parentId: 28,
          active: false,
        },
        {
          path: '/forms/upload',
          name: 'File Upload',
          component: {
            _status: -1,
            _result: null,
          },
          id: 34,
          parentId: 28,
          active: false,
        },
      ],
      id: 28,
      active: false,
    },
    {
      path: '/tables',
      name: 'Tables',
      icon: {
        propTypes: {},
        displayName: 'Grid',
      },
      children: [
        {
          path: '/tables/basic',
          name: 'Basic',
          component: {
            _status: -1,
            _result: null,
          },
          id: 36,
          parentId: 35,
          active: false,
        },
        {
          path: '/tables/advanced',
          name: 'Advanced',
          component: {
            _status: -1,
            _result: null,
          },
          id: 37,
          parentId: 35,
          active: false,
        },
      ],
      id: 35,
      active: false,
    },
  ],
}

const USER_TYPE = {
  EDITOR: 'EDITOR',
  ADMIN: 'ADMIN',
  VIEWER: 'VIEWER',
}

const CookieKey = {
  accessToken: 'accessToken',
  currentRoles: 'CurrentRoles',
  previousUrl: 'previousUrl',
  networkError: 'networkError',
}

const ROUTER = {
  Home: '/',
  PageNotFound: '/404',
  Login: '/auth',
  Dashboard: '/order',
}

const AUTH_PAGE = {
  LOGIN: 'LOGIN',
  SIGN_UP_EMAIL: 'SIGN_UP_EMAIL',
  SIGN_UP_PASSWORD: 'SIGN_UP_PASSWORD',
  FORGOT_PASSWORD: 'FORGOT_PASSWORD',
  RESET_PASSWORD: 'RESET_PASSWORD',
  VERIFY_EMAIL_REGISTER: 'VERIFY_EMAIL_REGISTER',
  VERIFY_EMAIL_RESET_PASSWORD: 'VERIFY_EMAIL_RESET_PASSWORD',
  REGISTER: 'REGISTER',
  ALERT: 'ALERT',
}

const REGEX = {
  PASSWORD: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/,
  PASSWORD_AUTH: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,
}

export {DefaultMenu, USER_TYPE, CookieKey, ROUTER, AUTH_PAGE, REGEX}
