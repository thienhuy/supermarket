export const API_URL = 'https://truly-contacts.herokuapp.com/api/'
export const {SENTRY_DSN} = process.env
export const {APP_ENV} = process.env
