import Cookies from 'universal-cookie'
import {addHours, addMonths} from 'date-fns'
import {getCurrentDomain} from '../utils'
import {CookieKey} from '../constant/common'

const cookies = new Cookies()
export const CookiesStorage = {
  getCookieData(key: string) {
    return cookies.get(key)
  },
  setCookieData(key: string, data: string) {
    const domain = getCurrentDomain()
    const currentTime = new Date()
    const expires = addMonths(currentTime, 1)
    cookies.set(key, data, {domain, expires, path: '/'})
  },
  clearCookieData(key: string) {
    const domain = getCurrentDomain()
    cookies.remove(key, {domain, path: '/'})
  },

  getAccessToken() {
    return cookies.get(CookieKey.accessToken)
  },

  setAccessToken(accessToken: string) {
    const domain = getCurrentDomain()
    const currentTime = new Date()
    const expires = addHours(currentTime, 1)
    cookies.set(CookieKey.accessToken, accessToken, {
      domain,
      path: '/',
      expires,
    })
  },
  clearAccessToken() {
    const domain = getCurrentDomain()
    cookies.remove(CookieKey.accessToken, {domain})
  },

  authenticated() {
    const accessToken = cookies.get(CookieKey.accessToken)
    return accessToken !== undefined
  },
}
