import {Box, CircularProgress, Container, Link, Typography} from '@mui/material'
import {styled} from '@mui/material/styles'
import PropTypes from 'prop-types'
import React, {useEffect, useState} from 'react'
import {contactApi} from '../../../api/contactApi'
import {Contact} from '../../../models'
import CustomForm from './element/CustomForm'

const ContentStyle = styled('div')(({theme}) => ({
  width: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0),
}))

// ----------------------------------------------------------------------
interface Props {
  onDone: () => void
  id: string
}

export default function EditUserComponent({id, onDone}: Props) {
  const [contact, setContact] = useState<Contact>()

  const isEdit = Boolean(id)

  useEffect(() => {
    if (!id) return // IFFE
    ;(async () => {
      try {
        const data: any = await contactApi.getById(id)
        setContact(data)
      } catch (error) {
        console.log('Failed to fetch contact details', error)
      }
    })()
  }, [id])

  const handleStudentFormSubmit = async (formValues: Contact) => {
    // TODO: Handle submit here, call API  to add/update student
    if (isEdit) {
      try {
        await contactApi.update(formValues)
        onDone()
      } catch (err) {
        console.log(err)
      }
    } else {
      try {
        await contactApi.add(formValues)
        onDone()
      } catch (err) {
        console.log(err)
      }
    }
  }

  const initialValues: Contact = {
    id: '0',
    first_name: '',
    last_name: '',
    phone_number: '',
    contact_picture: '',
    is_favorite: false,
    ...contact,
  } as Contact

  return (
    <Container>
      <ContentStyle>
        <Box sx={{mb: 10}}>
          <Typography variant='h4' gutterBottom>
            Ads Admin System
          </Typography>
        </Box>

        <Typography variant='h6'>
          {isEdit ? 'Update contact info' : 'Add new contact'}
        </Typography>

        {!isEdit || Boolean(contact) ? (
          <Box mt={3}>
            <CustomForm
              initialValues={initialValues}
              onSubmit={handleStudentFormSubmit}
            />
          </Box>
        ) : (
          <Box sx={{display: 'flex'}}>
            <CircularProgress />
          </Box>
        )}

        <Typography
          variant='body2'
          align='center'
          sx={{color: 'text.secondary', mt: 3}}>
          Quản lý người dùng&nbsp;
          <Link underline='always' sx={{color: 'text.primary'}}>
            Dịch vụ khách hàng
          </Link>
          &nbsp;and&nbsp;
          <Link underline='always' sx={{color: 'text.primary'}}>
            Privacy Policy
          </Link>
          .
        </Typography>
      </ContentStyle>
    </Container>
  )
}

EditUserComponent.prototype = {
  onDone: PropTypes.func,
}
