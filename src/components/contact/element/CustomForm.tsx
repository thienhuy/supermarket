import { yupResolver } from '@hookform/resolvers/yup'
import { Favorite, FavoriteBorder } from '@mui/icons-material'
import {
  Box, Button,
  Checkbox,
  CircularProgress,
  Grid,
  Typography
} from '@mui/material'
import React, { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import * as Yup from 'yup'
import { Contact } from '../../../../models'
import { InputField } from '../../../components/FormFields'

export interface ContactFormProps {
  initialValues?: Contact
  onSubmit?: (formValues: Contact) => void
}

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const schema = Yup.object().shape({
  first_name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Name required'),
  last_name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Address required'),
  // is_favorite: Yup.boolean().oneOf(
  //   [true],
  //   'You must accept the terms and conditions'
  // ),
  phone_number: Yup.string().matches(phoneRegExp, 'Phone number is not valid'),
})

export default function CustomForm({
  initialValues,
  onSubmit,
}: ContactFormProps) {
  console.log('value', initialValues)
  const [_, setError] = useState<string>('')

  const {
    control,
    handleSubmit,
    formState: {isSubmitting},
  } = useForm<Contact>({
    defaultValues: initialValues,
    resolver: yupResolver(schema),
  })

  const handleFormSubmit = async (formValues: Contact) => {
    console.log('formValue', formValues)

    try {
      // Clear previous submission error
      setError('')
      await onSubmit?.(formValues)
    } catch (error: any) {
      setError(error.message)
    }
  }

  return (
    <Box maxWidth={400}>
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <Grid container spacing={3}>
          <Grid item sm={6}>
            <InputField
              name='first_name'
              control={control}
              label='First_name'
            />
          </Grid>
          <Grid item sm={6}>
            <InputField name='last_name' control={control} label='Last_name' />
          </Grid>
        </Grid>
        <Box>
          <Typography variant='body1'>Favorite: </Typography>
          <Controller
            control={control}
            name='is_favorite'
            render={({field: {value, onChange}}) => (
              <Checkbox
                checked={value}
                onChange={(e) => {
                  onChange(e.target.checked)
                }}
                icon={<FavoriteBorder />}
                checkedIcon={<Favorite />}
              />
            )}
          />
        </Box>

        <InputField
          name='phone_number'
          control={control}
          label='phone_number'
        />
       <InputField
          name='contact_picture'
          control={control}
          label='contact_picture'
        />

        <Box mt={3}>
          <Button
            type='submit'
            variant='contained'
            color='primary'
            disabled={isSubmitting}>
            {isSubmitting && <CircularProgress size={16} color='primary' />}
            &nbsp;Save
          </Button>
        </Box>
      </form>
    </Box>
  )
}
