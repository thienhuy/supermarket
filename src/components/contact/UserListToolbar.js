import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'
import FindReplaceIcon from '@mui/icons-material/FindReplace'
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted'
import {
  IconButton,
  InputAdornment,
  OutlinedInput,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material'
// material
import {styled} from '@mui/material/styles'
import PropTypes from 'prop-types'

// ----------------------------------------------------------------------

const RootStyle = styled(Toolbar)(({theme}) => ({
  height: 50,
  display: 'flex',
  justifyContent: 'space-between',
  padding: theme.spacing(0, 1, 0, 1),
}))

const SearchStyle = styled(OutlinedInput)(({theme}) => ({
  height: 50,
  width: 200,
  transition: theme.transitions.create(['box-shadow', 'width'], {
    easing: theme.transitions.easing.easeInOut,
    duration: theme.transitions.duration.shorter,
  }),
  '&.Mui-focused': {width: 240, boxShadow: '5px 10px 8px #888888'},
  '& fieldset': {
    borderWidth: `1px !important`,
    borderColor: `${theme.palette.grey[500_32]} !important`,
  },
}))

// ----------------------------------------------------------------------

export default function UserListToolbar({
  numSelected,
  filterName,
  onFilterName,
  onDelete,
}) {
  return (
    <SearchStyle
      value={filterName}
      onChange={onFilterName}
      placeholder='Search user name...'
      startAdornment={
        <InputAdornment position='start'>
          <FindReplaceIcon />
        </InputAdornment>
      }
    />
  )
}
UserListToolbar.propTypes = {
  numSelected: PropTypes.number,
  filterName: PropTypes.string,
  onFilterName: PropTypes.func,
  onDelete: PropTypes.func,
}
