import * as React from 'react'
import {useEffect, useState} from 'react'
import Container from '@mui/material/Container'
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from '@mui/material'
import {
  categoryActions,
  selectCategoryList,
} from '../../../../features/categories/categoriesSlice'
import {productsApi} from '../../../../api/productsApi'
// import {categoriesApi} from '../../../../api/categoriesApi'
import {Box} from '@mui/system'
import {useAppDispatch, useAppSelector} from '../../../../app/hooks'

interface Props {
  id: string
}
export default function ProductForm({id}: Props) {
  const [form, setForm] = useState<any>({})
  const [categoryId, setCategoryId] = useState<string>('')
  const dispatch = useAppDispatch()

  useEffect(() => {
    setCategoryId(form.category_id)
  }, [form.category_id])

  const handleChangeCategory = (event: SelectChangeEvent) => {
    setForm({...form, category_id: event.target.value})
    // Todo: tk bạn xem dùm t chỗ ni đổ ra sai mô rứa?
  }

  const categoryList = useAppSelector(selectCategoryList)

  console.log('categoryList', categoryList)
  const handleChange = (e: any) => {
    const {value, name} = e.target
    setForm({...form, [name]: value})
  }

  useEffect(() => {
    dispatch(categoryActions.fetchCategoryList())
    productsApi
      .getById(id)
      .then(({deletedAt, ...res}) => setForm(res))
      .catch((err) => console.log('err', err))
  }, [])

  const onClick = () => {
    const formData = new FormData()
    Object.entries(form).forEach(([name, value]) => {
      if (name === 'product_pictures') {
        formData.append(name, JSON.stringify(value))
      } else if (name === 'file[]') {
        for (let file of value as File[]) {
          formData.append('file[]', file)
        }
      } else {
        formData.append(name, value as any)
      }
    })

    productsApi
      .update(id, formData as any)
      .then((res) => console.log('res', res))
  }
  const handleFileChange = (e: any) => {
    setForm({...form, 'file[]': e.target.files})
  }

  const handleDeleteImage = (image: string) => {
    const {product_pictures} = form
    const newList = product_pictures.filter((item: string) => item != image)
    console.log('new', newList)
    setForm({
      ...form,
      product_pictures: newList,
    })
  }

  const {
    product_name,
    unit_id,
    description,
    quantity,
    product_pictures,
    price,
  } = form

  console.log('form', form)
  console.log('picture', product_pictures)

  return (
    <Container maxWidth='sm'>
      <TextField
        name={'product_name'}
        label='Product name'
        onChange={handleChange}
        value={product_name}
      />
      {/* <TextField
        name={'category_id'}
        label={'Category id'}
        onChange={handleChange}
        value={category_id}
      /> */}
      <Box sx={{}}>
        <FormControl sx={{width: '80%'}}>
          <InputLabel id='demo-simple-select-label'>Categories</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            value={categoryId}
            label='CategoriesID'
            onChange={handleChangeCategory}>
            {categoryList.map((item: any) => (
              <MenuItem key={item.id} value={item.id}>
                {item.category_name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <TextField
        name={'unit_id'}
        label={'Unit id'}
        onChange={handleChange}
        value={unit_id}
      />
      <TextField
        name={'price'}
        label={'Price'}
        onChange={handleChange}
        value={price}
      />
      <TextField
        name={'description'}
        label={'Description'}
        onChange={handleChange}
        value={description}
      />
      <TextField
        name={'quantity'}
        label={'Quantity'}
        onChange={handleChange}
        value={quantity}
      />
      <label htmlFor='contained-button-file'>
        <input
          style={{display: 'none'}}
          accept='image/*'
          id='contained-button-file'
          multiple
          type='file'
          onChange={handleFileChange}
        />
        <Button variant='contained' component='span'>
          upload more
        </Button>
      </label>
      {product_pictures?.map((image: any) => (
        <div key={image}>
          <img src={'http://localhost:3000' + image} width={300} height={300} />
          <Button onClick={() => handleDeleteImage(image)}>X</Button>
        </div>
      ))}
      <Button onClick={onClick}>Submit</Button>
    </Container>
  )
}
