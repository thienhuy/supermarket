import {CircularProgress, Container, Link, Typography} from '@mui/material'
import {Box, styled} from '@mui/system'
import {useEffect, useState} from 'react'
import {productsApi} from '../../../api/productsApi'
import {Product} from '../../../models'
import ProductForm from './element/ProductForm'
import PropTypes from 'prop-types'

const ContentStyle = styled('div')(({theme}) => ({
  width: 560,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0),
}))

// ----------------------------------------------------------------------
interface Props {
  onDone: () => void
  id: string
}

export default function ProductComponent({id, onDone}: Props) {
  const [product, setProduct] = useState<Product>()

  const isEdit = Boolean(id)

  useEffect(() => {
    if (!id) return // IFFE
    ;(async () => {
      try {
        const data: any = await productsApi.getById(id)
        setProduct(data)
      } catch (error) {
        console.log('Failed to fetch contact details', error)
      }
    })()
  }, [id])

  //   const handleProductFormSubmit = async (formValues: Product) => {
  //     // TODO: Handle submit here, call API  to add/update student
  //     if (isEdit) {
  //       try {
  //         await productsApi.update(formValues)
  //         onDone()
  //       } catch (err) {
  //         console.log(err)
  //       }
  //     } else {
  //       try {
  //         await productsApi.add(formValues)
  //         onDone()
  //       } catch (err) {
  //         console.log(err)
  //       }
  //     }
  //   }

  //   const initialValues: Product = {
  //     id: '0',
  //     product_name: '',
  //     price: '',
  //     description: '',
  //     contact_picture: '',
  //     is_favorite: false,
  //     ...contact,
  //   } as Product

  return (
    <Container>
      <ContentStyle>
        <Box sx={{mb: 10}}>
          <Typography variant='h4' gutterBottom>
            Ads Admin System
          </Typography>
        </Box>

        <Typography variant='h6'>
          {isEdit ? 'Update product info' : 'Add new product'}
        </Typography>

        {!isEdit || Boolean(product) ? (
          <Box mt={3}>
            <ProductForm
              id={id}
              //   initialValues={initialValues}
              //   onSubmit={handleProductFormSubmit}
            />
          </Box>
        ) : (
          <Box sx={{display: 'flex'}}>
            <CircularProgress />
          </Box>
        )}

        <Typography
          variant='body2'
          align='center'
          sx={{color: 'text.secondary', mt: 3}}>
          Quản lý sản phẩm&nbsp;
          <Link underline='always' sx={{color: 'text.primary'}}>
            Pi SuperMarket
          </Link>
          &nbsp;and&nbsp;
          <Link underline='always' sx={{color: 'text.primary'}}>
            Privacy Policy
          </Link>
          .
        </Typography>
      </ContentStyle>
    </Container>
  )
}

ProductComponent.prototype = {
  onDone: PropTypes.func,
}
