import HomeIcon from '@mui/icons-material/Home'
import LogoutIcon from '@mui/icons-material/Logout'
import PersonOutlineIcon from '@mui/icons-material/PersonOutline'
import SettingsIcon from '@mui/icons-material/Settings'
import {
  Avatar,
  Box,
  Button,
  Divider,
  IconButton,
  MenuItem,
  Typography,
} from '@mui/material'
// import {Link as RouterLink} from 'react-router-dom'
// material
import {alpha} from '@mui/material/styles'
import {useRouter} from 'next/router'
import {useRef, useState, useEffect} from 'react'
import {useAppDispatch} from '../../../app/hooks'
import {authActions} from '../../../features/auth/authSlice'
import {User} from '../../../models/User'
import {CookiesStorage} from '../../../shared/config/cookie'
// components
import MenuPopover from './element/MenuPopover'

// ----------------------------------------------------------------------

const MENU_OPTIONS = [
  {
    label: 'Home',
    icon: () => <HomeIcon />,
    linkTo: '/',
  },
  {
    label: 'Profile',
    icon: () => <PersonOutlineIcon />,
    linkTo: '#',
  },
  {
    label: 'Settings',
    icon: () => <SettingsIcon />,
    linkTo: '#',
  },
]

// ----------------------------------------------------------------------

export default function AccountPopover() {
  const anchorRef = useRef(null)
  const [open, setOpen] = useState<boolean>(false)
  const dispatch = useAppDispatch()
  const router = useRouter()
  const [currentUser, setCurrentUser] = useState<User>()

  useEffect(() => {
    const user = CookiesStorage.getCookieData('user')
    if (user) {
      setCurrentUser(user)
    }
  }, [])
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  const handleLogout = () => {
    dispatch(
      authActions.logout({
        onSuccess() {
          router.push('/login')
        },
      })
    )
  }

  return (
    <>
      <Typography variant='subtitle1' noWrap>
        {currentUser?.username}
      </Typography>
      <IconButton
        ref={anchorRef}
        onClick={handleOpen}
        sx={{
          padding: 0,
          marginLeft: 2,
          width: 44,
          height: 44,
          ...(open && {
            '&:before': {
              zIndex: 1,
              content: "''",
              width: '100%',
              height: '100%',
              borderRadius: '50%',
              position: 'absolute',
              bgcolor: (theme) => alpha(theme.palette.grey[900], 0.72),
            },
          }),
        }}>
        <Avatar alt='photoURL' />
      </IconButton>

      <MenuPopover
        open={open}
        onClose={handleClose}
        anchorEl={anchorRef.current}
        sx={{width: 220}}>
        <Box sx={{my: 1.5, px: 2.5}}>
          <Typography variant='body2' sx={{color: 'text.secondary'}} noWrap>
            {currentUser?.first_name}
          </Typography>
        </Box>

        <Divider sx={{my: 1}} />

        {MENU_OPTIONS.map((option) => (
          <MenuItem
            key={option.label}
            onClick={handleClose}
            sx={{typography: 'body2', py: 1, px: 2.5}}>
            <Box
              sx={{
                // width: 24,
                height: 24,
              }}
            />
            {option.icon()}
            &nbsp;
            {option.label}
          </MenuItem>
        ))}
        <Divider sx={{my: 1}} />
        <Box sx={{p: 2, pt: 1.5}}>
          <Button
            fullWidth
            color='info'
            variant='outlined'
            startIcon={<LogoutIcon />}
            onClick={handleLogout}>
            Logout
          </Button>
        </Box>
      </MenuPopover>
    </>
  )
}
