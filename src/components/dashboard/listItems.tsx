import * as React from 'react'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import PeopleIcon from '@mui/icons-material/People'
import {ListItem, ListItemButton} from '@mui/material'

export const mainListItems = (
  <div>
    <ListItem>
      <ListItemButton onClick={() => console.log('user')} selected>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary='Contacts' />
      </ListItemButton>
    </ListItem>
    <ListItem>
      <ListItemButton onClick={() => console.log('product')} selected>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary='Products' />
      </ListItemButton>
    </ListItem>
  </div>
)
