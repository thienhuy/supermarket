import {all} from 'redux-saga/effects'
import {authSaga} from '../features/auth/authSaga'
import {contactsSaga} from '../features/contacts/contactsSaga'
import {productsSaga} from '../features/products/productsSaga'
import {categoriesSaga} from '../features/categories/categoriesSaga'

export default function* rootSaga() {
  yield all([authSaga(), contactsSaga(), productsSaga(), categoriesSaga()])
}
